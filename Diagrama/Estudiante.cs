﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diagrama
{
    class Estudiante: Usuario
    {
        private string materia;

        public string Materia
        {
            get { return materia; }
            set { materia = value; }
        }
        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        private int myVar;

        public int MyProperty
        {
            get { return myVar; }
            set { myVar = value; }
        }

        public Estudiante(string nombre, string apellido, string cedula, string tipo)
            : base(nombre, apellido, cedula, tipo)
        {

        }
    }
}
