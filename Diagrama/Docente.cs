﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diagrama
{
    class Docente : Usuario
    {
        public Docente (string nombre, string apellido, string cedula, string tipo)
          : base(nombre, apellido, cedula, tipo)
        {
            
        }
        private string materia;

        public string Materia   
        {
            get { return materia; }
            set { materia = value; }
        }
        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        private string curso;

        public string Curso
        {
            get { return curso; }
            set { curso = value; }
        }
        private List<Estudiante> estudiantes;

        public List<Estudiante> Estudiantes
        {
            get { return estudiantes; }
            set { estudiantes = value; }
        }



    }
}
